#!/usr/bin/gnuplot
# gnuplot -e "out='/home/gab/Bureau/zeta.png'" -e "data='/home/gab/Nextcloud/z.data'" -e "w=297*2" -e "h=210*2" zetag.plot
# Tout le tour du cadrant
# x=0 ; for z in {0..360..12} {12..360..12} {12..360..12} {12..360..12}; do printf -v rz "%03i" $z ; printf -v rx "%03i" $x ; x=$((x+3)); echo "$rx-$rz" ; gnuplot -e "out='/home/gab/Nextcloud/zeta-$rx-$rz.png'" -e "data='/home/gab/Nextcloud/z.data'" -e "w=297/2" -e "h=210/2" -e "rz=$z" -e "rx=$x"  zetag.plot 2>/dev/null ; done
# Un tour sur moi-même
# for r in {0..360..1} ; do printf -v rz '%03i' "$r" ; gnuplot -e "out='/Users/gab/Desktop/zeta-$rz.png'" -e "data='/Users/gab/Nextcloud/z.data'" -e "w=297/2" -e "h=210/2" -e "rz=$r" -e "rx=60" zetag.plot ; done

if (GPVAL_VERSION < 6.0) {
	print("Need gnuplot >= 6.0")
	exit(1)
}
print sprintf("%s",strftime("%H:%M:%S",time(0)))
set encoding utf8
dpi=300		#dpi
inch=25.3	#mm
pt=0.3528	#mm
#rz=65
#rx=60
# ##############################
if ( ! exists("tag")) { tag=""}
if ( ! exists("pal")) { pal="default"}
paldata=pal
if (pal eq "default") { paldata="rgb 7,5,15" ; }
if (pal eq "default") { paldata="model HSV rgb 3,2,2" ; }
if (pal eq "rainbow") { paldata="rgb 33,13,10" ; }
if (pal eq "ocean")   { paldata="rgb 23,28,3" ; }
if (pal eq "afm")     { paldata="rgb 34,35,36" ; }
if (pal eq "geo")     { paldata="defined ( 0 0 0.2 0, 0.1 0 0.4 0.1, 0.3 0.6 0.3 0, 0.5 0.8 0.6 0, 1 1 1 1 )" ; }
if (pal eq "cocktail"){ paldata="defined (0 0 0.32 0.55, 0.25 0 0.6 0.3, 0.6 0.7 0.6 0, 0.75 1 0.3 0, 1 1 1 0)" ; }
if (pal eq "meteo1")  { paldata="defined (0 0.9 0.9 1, 35 0.3 0.3 1, 50 0.6 0.15 0.4, 70 'red', 100 'yellow')"
if (pal eq "meteo2")  {	paldata="rgbformulae 34,-35,-36" ; }
if (pal eq "meteo3")  {	paldata="rgbformulae 22,13,-31" ; }
if (pal eq "meteo4")  {	paldata="defined (0 0 0.32 0.55, 0.25 0 0.6 0.3, 0.6 0.7 0.6 0, 0.75 1 0.3 0, 1 1 1 0)" ; }
}
if ( ! exists("size")) { size="A6" ; }
if (size eq "A10") { h=26 ; w=37 ; }
if (size eq "A9") { h=37 ; w=52 ; }
if (size eq "A8") { h=52 ; w=74 ; }
if (size eq "A7") { h=74 ; w=105 ; }
if (size eq "A6") { h=105 ; w=148 ; }
if (size eq "A5") { h=148 ; w=210 ; }
if (size eq "A4") { h=210 ; w=297 ; }
if (size eq "A3") { h=297 ; w=420 ; }
if (size eq "A2") { h=420 ; w=594 ; }
if (size eq "A1") { h=594 ; w=841 ; }
if (size eq "A0") { h=841 ; w=1189 ; }
wx=w*dpi/inch
hx=h*dpi/inch
#set border 1+2+4+8+16+32+64+128+256+512+1024
set border 1+2+4+8+16+32+64+256+512
set xrange [-17:7]
set yrange [-5:35]
set zrange [0:1.1]
set grid x y z #vertical
#set dgrid3d
set samples 100 ; set isosamples 500
unset ztics
set xyplane 0
set hidden3d

#unset surface
#set contour surface
#set cntrp levels incr 0,0.10,1.1
#set table $contours
#	splot "/mnt/c/Users/gabriel.guillon.p/Nextcloud/z.data"
#unset table
#unset contour
#set surface

#
# Contour
#
unset surface
set contour
set table $iso
#set zrange[-1:1.1]
# Harmonieusement répartie
#iso="0.01,0.04, 0.1, 0.2, 0.3, 0.4,0.5, 0.6, 0.7, 0.8, 0.9, 0.96, 0.995, 1.02, 1.09"
iso="0.01, 0.04, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.96, 0.996, 1.02, 1.09"
#set cntrparam levels incr 0,0.09,1.1
# Les iso ou les re pairs remontent
#set cntrparam levels discrete 0,0.2,0.3,0.4,0.6,0.7,0.8,0.9,1,1.1,0.0227307018487579,0.093714536225625,0.520525390778859
set cntrparam levels discrete @iso
#splot data
splot abs(zeta(x+I*y))
unset table
unset contour
############################

#
# Surface
#
set surface
set contour surface
set palette @paldata
# arc en ciel
#set palette rgb 33,13,10
# flashy
#set palette rgb 30,31,32
# rouge
#set palette rgb 21,22,23
set colorbox user size 0.02,0.3 origin 0.93,0.4
set style fill solid noborder
#set view 60,20,1.1,1
set view rx,rz,1.1,1
#set view 60,220,1.1,1

#set title "{/Times=16 Fonction zêta de Riemann}    {/Times:Italic=16 ζ(s) = {∑@_{/*0.5 {k=1}}^{/*.5 ∞}}  {/:Italic=12 k^{ -s}}"
#set title "{/Times:Italic=16 ζ(s) = {∑@_{/*0.5 {k=1}}^{/*.5 ∞}}  {/:Italic=12 k^{ -s}}"
set title "{/,*1.3:Italic ζ(s) = {∑@_{/*0.5 {k=1}}^{/*.5 ∞}}  {/:Italic k^{ -s}}" font "Times"
# Times,10 pour A2
set label "Korsani - 2024 - CC BY-NC-SA 4.0 DEED" at screen 0.90, screen 0.03 font "Times,10" textcolor rgb "black"
set cntrlabel font ",10"
set xlabel "Re(s)"
set ylabel "Im(s)"
set zlabel "|ζ(s)|"
#set key at screen 1, 0.9 right top vertical Right noreverse enhanced autotitle nobox
#set key autotitle nobox

#set key right top vertical Left noreverse enhanced autotitle
#set key nobox noopaque
#set key noinvert samplen 4 spacing 1 width 0 height 0
unset key

#
# Output
#
# Décommenter pour faire des gif
#unset xtics ; unset ytics ; unset colorbox
#unset xlabel ; unset ylabel ; unset zlabel
#unset border
#unset title
#unset label

out=sprintf("zeta-%s-%s-%s-%s-%03ir%03i%s.png",pal,size,data,ARG0,rx,rz,tag)
set output out

# Pour A1
if (size eq "A1") {
	set terminal pngcairo size wx,hx font "Courier New" fontscale pt*dpi/inch*1.5 linewidth pt*dpi/inch/3 pointscale pt*dpi/inch
}
# A2
if (size eq "A2") {
	set terminal pngcairo size wx,hx font "Courier New" fontscale pt*dpi/inch/1.5 linewidth pt*dpi/inch/3 pointscale pt*dpi/inch
}
set multiplot
# Pour les petites tailles
#set terminal pngcairo size wx,hx font "Courier New" fontscale pt*dpi/inch/3.5 linewidth pt*dpi/inch/5 pointscale pt*dpi/inch

#set terminal pngcairo size wx,hx font "Courier New" fontscale pt*dpi/inch/1.5 linewidth pt*dpi/inch/3 pointscale pt*dpi/inch
#set terminal png size wx,hx #font "Courier New,50"
#out=sprintf("zeta-%s-%s-%s-%s-%03i-%i-%i%s.png",pal,size,data,ARG0,rz,spl,isospl,tag)
print out

set pm3d clip z
set pm3d lighting primary 0.05 specular 0.1 spec2 0.1
set pm3d interpolate 10,10 lighting primary 0.05 specular 0.1 spec2 0.1

start=time(0)

##########################
# Réel
if (tag eq "-real") {
	splot $iso using 1:2:(0) with lines lc rgb "grey20"
	splot abs(zeta(x+I*y)) with pm3d notitle
	print sprintf("%s",strftime("%H:%M:%S",time(0)-start))
	exit
}

##########################
# Scatter
if (tag eq "-data") {
	#set cbrange [-pi:pi]
	#set output out
	#set multiplot
	#set size wx,hx
	splot $iso using 1:2:(0) with lines lc rgb "grey20"
	splot data using 1:2:3 with pm3d
	#set size 0.15,0.15
	#set origin 0.85,0.85
	#unset title
	#unset tics
	#unset label
	#unset xlabel
	#unset ylabel
	#unset zlabel
	#set view ,220
	#splot $iso using 1:2:(0) with lines lc rgb "grey20"
	#splot data with pm3d lw 0.6
	#unset multiplot
	print sprintf("%s",strftime("%H:%M:%S",time(0)-start))
	exit
}

##########################
# Gif/Webp
if (tag eq "-webp") {
	s=1
	n=0
	#set term webp animate delay (12000/(360/s)) size wx,hx
	#set output sprintf("zeta-%s.webp",size)
	start=time(0)+3600
	do for [ang = 0:360-s:s ] {
		rz=ang
		set view ,rz
		print sprintf("%s n=%i/%i rz=%i",strftime("%H:%M:%S",time(0)+3600),n+1,360/s,rz)
		out=sprintf("zeta-%s-%s-%s-%s-%03i%s.png",pal,size,data,ARG0,rz,tag)
		stats out nooutput
		if (GPVAL_ERRNO) {
			set output out
			set multiplot
			splot $iso using 1:2:(0) with lines lc rgb "grey20"
			#set style fill solid
			splot data with pm3d
			n=n+1
			eta=time(0)+(360/s-n)*(time(0)+3600-start)/n
			print sprintf("ETA: %s",strftime("%H:%M:%S",eta))
			unset multiplot
		}
	}
	unset output
	exit
}

#splot real(zeta(x+I*y)) with pm3d notitle
print sprintf("%s",strftime("%H:%M:%S",time(0)))
