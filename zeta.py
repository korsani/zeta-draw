#!/usr/bin/env python3
import cmath
import math
import numpy as np
import sys
import time
from mpmath import zeta
import argparse

parser = argparse.ArgumentParser(description='Zeta, baby')
parser.add_argument('-x',metavar="")
parser.add_argument('-y',metavar="")
parser.add_argument('-d',metavar="",help="Step between each calculated x and y values (-d=dx,dy). Can't be mixed with -n")
parser.add_argument('-n',metavar="",help="How many x and y points to calculate (-n=x,y). Can't be mixed with -d")
parser.add_argument('-zslice',metavar="",help="Draw only if z is close to (+/- -dz) those values")
parser.add_argument('-zfactor',metavar="",type=float,help="Multiply z by this value, eventually after -zclip")
parser.add_argument('-dz',metavar="",type=float,help="Size of the -zslice")
parser.add_argument('-zclip',metavar="",help="Clip z to those values. Default: -10,10")
parser.add_argument('-obj',action="store_true",help="Make an .obj file")
parser.add_argument('-obj_addfloor',action="store_true",help="and add a floor")
parser.add_argument('-obj_removeceiling',action="store_true",help="and remove the plateau")
parser.add_argument('-f',metavar="",help="Outfile. Default: stdout")
args=parser.parse_args()
if args.f is None:
    outfile=sys.stdout
else:
    outfile=open(args.f,"wt")
#zeta(1/2+14.134725141734693790457251983562470270784257115699243175685567460149j)
minx,maxx=args.x.split(',')
miny,maxy=args.y.split(',')
maxx=float(maxx); minx=float(minx)
maxy=float(maxy); miny=float(miny)
if args.zfactor is not None:
    zfactor=args.zfactor
else:
    zfactor=1
if args.n is not None:
    nx=int(args.n.split(",")[0])
    ny=int(args.n.split(",")[1])
    dx=(maxx-minx)/nx
    dy=(maxy-miny)/ny
else:
    dx=float(args.d.split(",")[0])
    dy=float(args.d.split(",")[1])
    # Easy way of ceiling
    nx=int(0.5+(maxx-minx)/dx)
    ny=int(0.5+(maxy-miny)/dy)
n=nx*ny
zslice=[]
zclip=[-10,10]
if args.zslice is not None:
    zslice=[float(i) for i in args.zslice.split(",")]
if args.dz is not None:
    dz=args.dz
if args.zclip is not None:
    zclip=[float(i) for i in args.zclip.split(",")]

ceiling=zfactor*zclip[1]
# Put ceiling high enough
if not args.obj_removeceiling:
    ceiling+=10

bsceiling=ceiling**4
print(f'# {args}',file=outfile)

# Current line number
line=0
# Number of x and y line
# Blocs:
# x1 y1
# x1 y2
#
# x2 y1
# x2 y2
linex=nx   # Number of x ( = number of blocks)
liney=ny   # Number of y ( = number of lines in a block)
vertices=liney*linex
print(f'{nx}x{ny}={vertices} vertices',file=sys.stderr)
print(f'surfaces are {dx}x{dy}',file=sys.stderr)
f='%(x)s %(y)s %(zeabs)s'
start=time.time()
start_vertices=time.time()
avertices=np.zeros((vertices,3))
for x in np.arange(minx,maxx,dx):
    for y in np.arange(miny,maxy,dy):
        if x == 1 and y == 0:
            continue
        ze=zeta(complex(x,y))
        zeabs=abs(ze)
        # Clipping
        if zeabs > zclip[1]:
            zeabs=zclip[1]
        if zeabs < zclip[0]:
            zeabs=zclip[0]
        zeabs=zeabs*zfactor
        #zemod=cmath.phase(ze)
        if args.zslice is None:
            avertices[line]=[x,y,zeabs]
            line+=1
        else:       # Only a slice
            for z in zslice:
                if z-dz <= zeabs <= z+dz:
                    avertices[line]=[x,y,zeabs]
                    line+=1
    progress=line/vertices
    duration=time.time()-start_vertices
    speed=line/duration
    print('\r%.02f%%, Speed=%iv/s, Started=%is ETA=%is ' % (100*progress,speed,duration,vertices/speed-duration),file=sys.stderr,end="")

# Faces, if -obj
start_faces=time.time()
if args.obj:
    # Max
    surfaces=int((line-liney)/liney*(liney-1))
    faces=np.zeros((surfaces,4))
    print("",file=outfile)
    print(f'{surfaces} surfaces max',file=sys.stderr)
    # Number of surfaces to take into account
    s=1
    # and number of displayed surfaces
    f=0
    # For each block, but not the last (each block contains 'liney' lines)
    for lx in np.arange(1,line-liney,liney):
        # I walk on each line of each block
        for ly in np.arange(lx,lx+liney-1,1):
            s+=1
            # If my 4 vertices goes beyond ceiling (the 4 z values are above)
            # no face
            # Yes, I don't remove the vertices
            if (avertices[ly-1][2]*avertices[ly][2]*avertices[liney+ly][2]*avertices[liney+ly-1][2]<bsceiling):
                # A rectangle that will link left-right
                faces[f]=[ly,ly+1,liney+ly+1,liney+ly]
                f+=1
        progress=s/surfaces
        duration=time.time()-start_faces
        speed=s/duration
        print('\r%.02f%%, Speed=%iv/s, Started=%is ETA=%is ' % (100*progress,speed,duration,vertices/speed-duration),file=sys.stderr,end="")
    # The end, displaying
    for v in avertices:
        print(f'v {v[1]} {v[2]} {v[0]}',file=outfile)
    for face in faces[:f]:
        print(f'f {int(face[0])} {int(face[1])} {int(face[2])} {int(face[3])}',file=outfile)
    if args.obj_addfloor:
        print('# Floor',file=outfile)
        for fx in (minx,maxx):
            for fy in (miny,maxy):
                print(f'v {fy} 0 {fx}',file=outfile)
                line=line+1
        print(f'f {line-1} {line} {line-2} {line-3}',file=outfile)
else:
    for p,v in enumerate(avertices):
        # Empty line between each block
        if (p%ny == 0):
            print("",file=outfile)
        print(f'{v[0]} {v[1]} {v[2]}',file=outfile)
print('\nDuration: %is' % (time.time()-start),file=sys.stderr)
