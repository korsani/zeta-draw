OS=$(shell uname)
echo=/usr/bin/echo
ifeq ($(OS),Darwin)
	echo=/bin/echo
endif
PLOT=zeta6.light.iso.gpl
DATA ?= z005.data
PALETTE ?= default
SIZE ?= A6
RZ ?= 65
GNUPLOT ?= docker run --rm --volume .:/work/ -w /work/ gnuplot:master
.PHONY: zeta

zeta:
	$(GNUPLOT) -e "data='$(DATA)';rx=60;rz=$(RZ);size='$(SIZE)';pal='$(PALETTE)';file='$(PLOT)'" "$(PLOT)"
	@$(echo) -e "\a"
