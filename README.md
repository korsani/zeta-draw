![Zeta](zeta.png)

# Drawing Riemann Zeta

I was looking for a nice representation of Riemann's Zeta function. What I found on Internet did not satisfy me: it wasn't looking nice.

So I made mine.

Here is what I used.

# Elements

## zeta.py

Python script to calculate zeta values.

It outputs a space separated file:

	real_part imag_part abs(zeta(real_part+i*imag_part))

To draw the graph in this doc, I used:

	$ zeta.py -x='-20,10' -y='-5,35' -d='0.05,0.05' -f z005.data

And it took ~5min on a fairly decent computer.

### 3D

It can also generate a `.obj` file if you put `-obj` option.

Those parameters generate a kind of boat:

	$ zeta.py -x="-19,7" -y="-15,15" -n=200,200 -zclip="0,7" -obj -f boat.obj -obj_removeceiling

## zeta.gp

`gnuplot(1)` (version >= 6.0) file to draw data from `zeta.py`

Usage:

	gnuplot -e "data='data_file';rx=rx_angle;rz=rz_angle;size='size';pal='palette';tag='tag'" "plot.gp"

Where:

	data	: data file to use
	rx	: x view angle
	rz	: z view angle
	size	: size of the generated graph. Should be one of A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10
	palette	: color palette to use. One of default, cubehelix, rainbow, ocean, afm, geo, cocktail, meteo1, meteo2, meteo3, meteo4
	tag	: tag to append the filename with. Can be anything, but special values are: "-data" to draw values from data file, "-real" to use gnuplot's zeta implementation, "-webp" to generate png images for an animation (rz will vary from 0 to 359), suitable for Imagick convert(1) to build an animated gif or webp. "-data" and "-real" both take aproximatively the same time, I found "-data" nicer

To build the graph in this doc, I used:

	gnuplot -e "data='z005.data';rx=60;rz=65;size='A2';pal='cubehelix';tag='-data'" "zeta.gp"

The gnuplot script is mainly for printing A2. Printing other sizes can render pretty hugly things.

# Considerations

My goal was to make a graphical representation of zeta complex values than can be printed and proudly put in my room: it should be nice to look (not boring, not hard to understand), accurate, and show the beauty of that functions: the non trivials zeros.

I found that abs(zeta) has a better look than Re(zeta).

The -5 35 y axis shows few non trivials zeros, but not too much so that the "big belly" has enough space. It also show the inner part of the "big belly".

The -20 10 x axis shows the "big belly" but not the uninteresting "desert land" when Re > 10 and when Re < -20

The -1 1.1 z axis shows zetas zeros but cut uninteresting "high summits" and "plateaus" of 1 and Im > 5 , Re < 0

X and Z view angle show the inner "big belly", few non trivials zeros, the "tip tap" of the zeros when Re is even and do not give too much importance to the different parts of the graph.

Color palette was choosen so that it looks like ocean, or mountains: your instinctively know that dark is deep.

Contours (level lines) were choosen to mimic waves and show that values when Re > 0 goes high.

Results is a pictures that evoque boats in harbour, a nepanthes, or a geological representation.

# See also

Some useful resources to understand Zeta Function (in french)

- By Axel Arno : https://www.youtube.com/watch?v=KKoxMe2T7zo
- By El JJ : https://www.youtube.com/watch?v=dNpdMYB8pZs
- By ScienceEtonnante : https://www.youtube.com/watch?v=KvculWl-jhE
- By Arte : https://www.youtube.com/watch?v=02rHA_Ay13M

# Copyright

![CC BY-NC-SA 4.0 DEED](by-nc-sa.eu.png)

https://creativecommons.org/licenses/by-nc-sa/4.0/

